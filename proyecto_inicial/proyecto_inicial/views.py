from datetime import datetime
from pathlib import Path

from django.http import HttpResponse
import os

from django.shortcuts import render
from django.template import Template, Context, loader

BASE_DIR = Path(__file__).resolve().parent.parent

def saludo(request):
    return HttpResponse("Hola mundo")

def saludo2(request, id):
    # documento = open(os.path.abspath("proyecto_inicial/plantillas/hola2.html"))
    documento = open(BASE_DIR / "proyecto_inicial/plantillas/hola2.html")
    # documento = open("C:/Users/Usuario2080/Desktop/django/proyecto_inicial/proyecto_inicial/plantillas/hola2.html")
    tlp = Template(documento.read())
    documento.close()
    ctx = Context({"nombre": "Carlos", "id": id})
    doc = tlp.render(ctx)
    return HttpResponse(doc)

# Imprime fechas y listas!!!!
def saludo3(request, id):
    ahora = datetime.now()
    lista = ["Carlos", "Julio", "Pedro"]
    # documento = open(os.path.abspath("proyecto_inicial/plantillas/hola3.html"))
    documento = open(BASE_DIR / "proyecto_inicial/plantillas/hola3.html")
    # documento = open("C:/Users/Usuario2080/Desktop/django/proyecto_inicial/proyecto_inicial/plantillas/hola3.html")
    tlp = Template(documento.read())
    documento.close()
    diccionario = {
        "nombre": "Carlos",
        "id": id,
        "fecha": str(ahora.day)+"-"+str(ahora.month)+"-"+str(ahora.year),
        "lista": lista
    }
    ctx = Context(diccionario)
    doc = tlp.render(ctx)
    return HttpResponse(doc)

# Imprime fechas y listas!!!!
def saludo4(request, id):
    ahora = datetime.now()
    lista = ["Carlos", "Julio", "Pedro"]
    diccionario = {
        "nombre": "Carlos",
        "id": id,
        "fecha": str(ahora.day)+"-"+str(ahora.month)+"-"+str(ahora.year),
        "lista": lista
    }
    documento = loader.get_template("hola4.html")
    doc = documento.render(diccionario)
    return HttpResponse(doc)

# Imprime fechas y listas!!!!, incrustando plantillas
def saludo5(request, id):
    ahora = datetime.now()
    lista = ["Carlos", "Julio", "Pedro"]
    diccionario = {
        "nombre": "Carlos",
        "id": id,
        "fecha": str(ahora.day)+"-"+str(ahora.month)+"-"+str(ahora.year),
        "lista": lista
    }
    return render(request, "hola4.html", diccionario)

# Con herencia de plantillas
def saludo6(request):
    ahora = datetime.now()
    lista = ["Carlos", "Julio", "Pedro"]
    diccionario = {
        "nombre": "Carlos",
        "fecha": str(ahora.day)+"-"+str(ahora.month)+"-"+str(ahora.year),
        "lista": lista
    }
    return render(request, "hola5.html", diccionario)

def despedirse(request):
    return HttpResponse("Adios mundo")