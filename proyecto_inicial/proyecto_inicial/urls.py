"""proyecto_inicial URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from proyecto_inicial.views import despedirse, saludo, saludo2, saludo3, saludo4, saludo5, saludo6

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hola/', saludo),
    path('saludo/<int:id>', saludo2),
    path('saludo2/<int:id>', saludo3),
    path('hola2/<int:id>', saludo4),
    path('hola3/<int:id>', saludo5),
    path('hola4.html', saludo6),
    path('despedida/', despedirse),

]

