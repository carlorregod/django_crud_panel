from django.http import HttpResponse
from django.shortcuts import render, redirect

from gestionPedidos.models import *

# Create your views here.

def busqueda_productos(request):
    return render(request, 'busqueda_productos.html')

def buscar(request):
    if request.method == 'POST':
        if request.POST['prd']:
            mensaje = "Artículo buscado: %r" %request.POST['prd']
            art = request.POST['prd']
            if len(art) > 25:
                return render(request, 'resultados_busqueda.html', {'articulos': [], 'query': art, 'error':'Ha ingresado más de 25 caracteres'})
            else:
                articulo = Articulo.objects.filter(nombre__icontains=art)  # nombre__icontains busca como un un like
                return render(request, 'resultados_busqueda.html', {'articulos': articulo, 'query': art, 'error': ''})
                # return HttpResponse(mensaje)
    return redirect('buscar_producto')


def contacto(request):
    return render(request, "contacto.html")

def contactar(request):
    if request.method == "POST":
        return render(request, "gracias.html")
    return render(request, "contacto.html")



