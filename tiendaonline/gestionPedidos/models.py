from django.db import models

class Cliente(models.Model):
    nombre = models.CharField(max_length=30, null=True)
    direccion = models.CharField(max_length=50, verbose_name='Domicilio')
    email = models.EmailField(blank=True, null=True)
    telefono = models.CharField(max_length=10)
    def __str__(self):
        return f"Nombre: {self.nombre}, Domicilio: {self.direccion}, Email: {self.email}, Telefono: {self.telefono} "
    # Personalice el nombre de una tabla
    class Meta:
        db_table = "clientes"


class Articulo(models.Model):
    nombre=models.CharField(max_length=30)
    seccion = models.CharField(max_length=20)
    precio = models.IntegerField()
    def __str__(self):
        return "Nombre: %s, Seccion: %s, Precio: %s "% (self.nombre, self.seccion, self.precio)
    class Meta:
        db_table = "articulos"


class Pedido(models.Model):
    numero = models.IntegerField()
    fecha = models.DateField()
    entregado = models.BooleanField()
    def __str__(self):
        return f"Número de pedido: {self.numero}, Fecha: {self.fecha}, Entregado: {self.entregado} "
    class Meta:
        db_table = "pedidos"
