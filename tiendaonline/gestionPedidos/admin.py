from django.contrib import admin

# Register your models here.
from gestionPedidos.models import *

# Personalizar tabulacion de registros en el panel de admin
class ClientesAdmin(admin.ModelAdmin):
    list_display = ("nombre", "direccion", "telefono") # Muestra tabular en el panel
    search_fields = ("nombre", "telefono")  # Agrega barra de búsqueda, buscará las columnas señaladas

class ArticulosAdmin(admin.ModelAdmin):
    list_display = ("nombre", "seccion", "precio") # Muestra tabular en el panel
    search_fields = ("nombre", "seccion")  # Agrega barra de búsqueda, buscará las columnas señaladas
    list_filter = ("seccion",) # Filtros

class PedidossAdmin(admin.ModelAdmin):
    list_display = ("numero", "fecha", "entregado") # Muestra tabular en el panel
    search_fields = ("numero", "fecha")  # Agrega barra de búsqueda, buscará las columnas señaladas
    list_filter = ("fecha","entregado") #Filtros misceláneos
    date_hierarchy = "fecha" #Filtro horizontal de fechas solamente

# admin.site.register(Modelo, ClasePropiedadesAdminModel)
admin.site.register(Cliente, ClientesAdmin)
admin.site.register(Articulo, ArticulosAdmin)
admin.site.register(Pedido, PedidossAdmin)

#Usuario personal
#carlos
#corrego69